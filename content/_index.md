---
title: "home"
date: 2022-02-01
draft: false
description: "Ethan Myers Home Page"
tags: ["ethan", "ethanmyers", "ethan myers", "myers", "ethan gerrard myers"]
---

# hello, world

Hi, my name is Ethan Myers.

This is a small site I put together to showcase my projects, keep a public reading list, and host a few essays I've written. The site also serves to keep me accountable to my projects, share progress, and help me solicit feedback on my writing.

My résumé is viewable [here](/pdf/CV_US.pdf); I can be reached at <ethan@ethanmye.rs>.

The RSS link is [here](/index.xml). If you'd share your email with me, you can do so [here](/email).

The site is built with [Hugo](https://gohugo.io/). It's hosted by [me](https://ethan.id) and obeys your operating system's [theme settings](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme). The source code is viewable [here.](https://gitlab.com/ethanmye-rs)
