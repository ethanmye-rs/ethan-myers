---
title : "The Traitor and the Spy"
draft : false
date: 2020-02-23T21:24:37-07:00
menu : "books"
---

**The Traitor and the Spy**, *Ben Macintyre*

The fascinating story of Oleg Gordievsky, KGB turned MI6 double agent. 

The subtitle of this book is "The Greatest Espionage Story of the Cold War", and it's without a doubt the greatest (public) espionage story I've ever heard. The book is an absolutely gripping story of Gordievsky's initial rise, courtship with British intelligence in Denmark, and rise thru the KGB to a colonel and [rezident](https://en.wikipedia.org/wiki/Resident_spy) of the KGB's London branch. The later parts of the story detail his perilous recall to Moscow, drug induced interrogation, and eventual exfiltration.

Discussed in parallel to Gordievsky are Aldrich Ames, CIA turned KGB double agent, and Kim Philby, MI6 turned KGB double agent. Ames more so than Philby, each is contrasted against Godievsky's motivation, actions, and fate.

Compared to any other part of the book, I found the exfiltration very interesting. Apparently, Margret Thatcher had taken a personal liking to the spy she referred to only as "Mr.Collins" and personally authorized the plan to extract him from Russia, which involved a 900+km drive to the Finnish border and a newborn.  This entire scheme was made possible only by expert level contingency planning, as well as gross incompetence and lethargy on the part of the KGB.

Overall, it's a wonderful book and a quick read, and I would highly recommend it.

