---
Title: BS
date: 2020-12-27
menu: "writing"
draft: true
---

*This is an essay I wrote while procrastinating on writing a different, less enjoyable essay. It's silly and irreverent*

I want to talk about bullshit — the volume, consistency, and source, specifically.

Bullshit is a funny thing; it’s slippery and amorphous, metastasizing and mutating incessantly. And yet, 9 times out of 10, people can see it, pick it out, and call it out. Why is something that defies all rigid definitions apparently so obvious?

The simple answer: bullshit isn’t obvious. Plainly put, the sheer quantity of bullshit begets miscatorgaztion. For every obvious piece of bullshit, so much more slips through even the finest of logical sieves, freely invading thoughts and polluting psyches. It’s the stuff of stereotypes and of heuristics, for better or worse.

Fortunately, bullshit, effectively stymied and damned, is recognizable and refutable by nearly all. So much of coming up with the right shit means exploring, dismantling, and Frankenstein-ing together disparate factoids and smidgens of bullshit into a gleaming monument to objective truth. Science, advancing one funeral at a time, is a cemetery littered with the bodies of wrong works past, epitaphs covered in false theory.

Unchecked, a trickle of bullshit becomes a deluge.

2021 is the year of the ox, and if 2020 is any indication, an excellent year for some bullshit. Social media, (specifically low barrier to entry social media) is by far the biggest purveyor. Twitter, Facebook et al. may promulgate restrictions on hate, hoax, and harassment, but that much-maligned triumvirate funds stratospheric valuations with incredible user engagement and decidedly juicy content. In the internet of beefs, hate is excellent corporate strategy.

I think I would be remiss if I didn’t talk about the particular shift in the content of bullshit. In my opinion, it’s gotten all the more sticky. Being correct, being intellectually honest, being careful — it’s all very boring and not terribly good for going viral. It’s much easier to digest, quote, and weaponize out-of-context factoids to support a preconceived world view, and in the year of the ox, there’s certainly plenty to sift through. 

Intrinsically linked to the shift in content is a shift in source. Far gone are the FWD:FWD:FWD:FWD s from Grandma; today’s bullshit is targeted, carefully A/B tested, and produced in unassailable volume by single sources. It is a wolf in sheep’s clothes, but in a world where oh-so-hungry wolves far outnumber sheep. Bullshit is an industry, a national strategy, and an effective tool of war. In a nation that values and protects the freedom of speech of all, I think individuals must staunch the flow, continuously cognizant of the motivations lurking below seemingly innocuous content. They must be aggressive in their denouncement and persuasive in their renunciation, and perhaps most importantly, they must adapt to the new, waist-deep reality we all share.

In this penultimate paragraph, I should probably interject. This was supposed to be a story, but I think stories are Very Bad™ ways to honestly get a point across. They’re highly mutable and will serve any master — they can transcend culture, misappropriated to new masters even before the progenitors are dead. They are rife with reader bias; they are impossible to refactor into something that means the same thing to everyone. They are weaponizable, they are prone to error with little in the way of error correction, they are overly verbose — the list goes on. 
