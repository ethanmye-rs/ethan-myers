---
title: National Merit Essay
date: 2019-12-27
menu: "writing"
---

*This is an essay I wrote for a college scholarship. I like it.*

One time, I accidentally burnt both my eyebrows off my face.

I’ve been building things since I figured out how to use a screwdriver -- even if that sometimes meant a fireball or two. With time, money, and generous help from teachers and strangers on the Internet, I’ve had the pleasure of building all sorts of things, only of which the most interesting can be squeezed onto the page.

A sputtering gun, depositing a cloud of vaporized metal onto a substrate, like glass, using a few thousand volts and a hard vacuum. I couldn’t get the indium tin oxide for an LCD, so I opted for sputtering some copper onto a sheet of glass to make a semi-transparent resistor.

An X-ray machine, using a 12V power source instead of 120V AC for portability, making the design much more complex. It operates at about 25 kHz, with two IRF540 MOSFETs forming a half bridge inverter to drive the primary. The MOSFETs have a bit of driving circuitry behind them, mainly a AB amplifier to drive the high gate capacitance, and a smattering of bulk and decoupling capacitance. The transformer is pulled from an old black and white CRT TV, which puts out approximately 12,000V AC. This is fed into a four stage voltage multiplier, for about 40,000V DC. This is then fed into an old soviet vacuum tube operated in cold cathode mode, which emits X-Rays via field emission. It’s not great, but it will produce an X-Ray exposure over the span of a few minutes and is much cheaper than what’s currently available.

A USB powered Geiger counter (So I could figure out just how dangerous that X-ray machine was.)

To fund all it all (and avoid working retail), a small business buying computer chips from China. I stay up late to ship orders, and even later to argue with my Chinese suppliers about bulk pricing ePacket rates. I’ve sold parts to SLAC (Stanford’s particle accelerator), Valve Research, and about 15 universities among the thousand-odd orders so far.

I’ve picked up a diverse skillset on the way, from MIG welding to OpenSCAD to C. I’ve met tons of cool people, from the guys at my local dump who let me salvage parts for coffee to the grizzled old NORAD contractor I bought my oscilloscope from.

The challenges that each project provides is stimulating and engrossing in a way school never was for me. I’ve learned not only the technical skills, but the interpersonal skills to ask for help, and to share my projects with the world. It’s an incredible thing to see something designed on a screen come to life, and it means even more when I can share it with the world. 


My eyebrows are back, by the way.
