---
title: Leadership
date: 2020-12-27
menu: "writing"
---

*This is an essay I wrote a some point, but I can't seem to remember. It doesn't help it's untitled*

The value of a leader is strictly a function of what their followers achieve. Even the most charismatic, inspirational and revolutionary leaders are worth nothing without followers. Thus, the most valuable characteristics for a leader are those that both attract followers and unite them behind a cause. 

Fundamentally, leadership starts with humility. The capacity to be humble may stand apart from the classic, narcissistic type A that has come to gobble up modern day leadership, but humility remains a fundamental element in the makeup of a leader. In its absence, a leader will not necessarily alienate supporters (not everyone dislikes a strongman), but will alienate critical supporters -- those with both the ability and desire to significantly advance the cause. Without those supporters, movements stagnate, and leaders fade into the fray. Humility prevents this alienation, as leaders listen to critical supporters to alter and redirect the movement.

Not to sacrifice one trait at the altar of another, a leader must have his or her own vision. Irrespective of whether this is shared, irrespective of whether it is agreed upon, irrespective of whether it is a achievable, a leader must have their own vision. Without it, the movement fails to advance -- no group is homogenous; a leader without vision will see his coalition splinter and crack. A leader without vision cannot incorporate the suggestions of critical supporters -- It is nearly impossible to rally around something that doesn’t exist.

A leader can be humble in a meeting with trusted advisors, yet put on a dictatorial tour de force in front of lay supporters without any hyprcrisy on his or her part. The key, however, is knowing when to use each, and is what separates a capable leader from an inept one. 

Finally, the third trait -- knowledge, or the capacity to learn. A leader must be knowledgeable about the area he or she seeks to lead. No combination of advisors, canned speeches, nor teleprompters can avoid this reality. Like humility, a lack of knowledge alienates key supporters (even if not necessarily a critical mass of supporters), befuddling the movement’s objectives and opening up the leader to abuse at the hands of those who are knowledgeable. Without knowledge, a leader can have neither vision nor humility, making knowledge the lynchpin of a  successful leader.

As important as these traits are, they are difficult to instill mechanically. A person can be wrought into a more humble version of themselves, but out from underneath the hot hammer blows, original shape will return. Vision can be crammed down the throats of those who lack it, yet it will never rise up to the eyes. Knowledge can be amassed into a tome worthy of beating upside the head, but knowledge cannot crawl from the pages into the brain. Each trait must be originally expressed by a prospective leader, carefully nourished and exercised by a mentor capable of recognizing said talent. This combination of nature and nurture is what produces a genuine leader. Unfortunately, in a world so enamored with the buzzwords, the true nature of leadership has been bastardized into a shell of its original form. Ultimately, a true leader must have the knowledge, vision, and humility to rise up and lead their movement to its goals. 

