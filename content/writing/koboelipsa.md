---
title: Kobo Elipsa Review
date: 2021-07-21
menu: "writing"
description: "My review of the Kobo Elipsa e-reader"
tags: ["kobo elipsa", "ereader", "eink", "pdf ereader", "e-reader", "e-reader pdf"]
draft: true
---

*I recently bought an ereader for reading large format PDFs. If you read a lot of papers or textbooks, this is a wonderful device.*

This is a short review of a new e-reader I pre-ordered and received in early July of 2021.

## use case, context

I read a lot of papers, textbooks, and other large format documents, almost exclusively available in pdf format. Unlike most novels, these pdfs are usually resistant to resizing and reformatting.

I used to read most of them on my laptop, but the experience is subpar. It's difficult to read in bed, bad at night, and I can't take notes like I would on a hard copy in the margins. I've also tried reading on a kindle, which works for most novels but fails miserably for anything larger format.


## Why this device

I saw devices like the remarkable (and remarkable 2), but had some concerns about software maturity and hardware maturity. I also balked a bit at the cost -- with a pencil and cover, I was going to pay almost $600.

I also looked at some other manufacturers, but wanted a product I won't have to google translate everything for.

The device itself is about the size of a sheet of paper, and comes with a stylus and cover, all for $400, less $25 if you use the code xxx

## experience

The device i
## some things I want to change

