---
title: Enguhlishishness
date: 2022-01-04
menu: "writing"
draft: false
---

*I know this essay switches between complicated and clear English. I like writing twisty, non-standard English, but this is for me, thus*

## Background
I think this essay makes more sense if I explicitly mention what spurred these thoughts. As a reminder to me:
- I'm learning Mandarin (我只会说一点儿中文!)
- I'm ~9 months into a job with Deloitte.
- I watched a Netflix movie ("Don't Look Up") that made me think of a book ("Laughing Ourselves to Death") I read.

## Write clear English
As important as the background is, the actual impetus for this essay is a tidbit I heard on NPR -- non-native speakers of English can have a great conversation amongst themselves, but when a native English speaker joins, understanding drops overall. I see how this might happen -- English is chockfull of parables that twist and turn like mountain roads; What, exactly, does it mean to hit a home run on a assignment? Touch base? Circle back? Sync up? Bubble up? Lay bare? (This completely ignores the insanity that is two/to/too, there/their, and the fact "Buffalo buffalo Buffalo buffalo buffalo buffalo Buffalo buffalo" is a valid English sentence). Ultimately, English has a lot of non-standard usage, in-jokes, and complicated phrases that plenty of native speakers don't understand. It's unreasonable to expect non-native speakers to understand them, yet these phrases infect native speakers and reduce understanding for everyone.

Because of this, in most professional conversations, I switch to a more direct English. This means cutting out about 70% of "fun" vocab, using subject+verb+object structures, and defenestrating all analogies/sayings/aphorisms/parables etc. Of course, English is a flexible language, so I prefer to do away with the straitjacket when not writing for a professional audience. It is important to remember, however, that professional English is not necessarily "formal" English. "lol", "btw", "ty", "ok" are commonly understood by a surprising number of non-English speakers, and imply a much less formal tone than is possible to convey in clear English.

Speaking of tone, controlling it is incredibly hard. Tone in person can be difficult enough to control, but at least there is body language to convey true intention. For example, in texting, I unconsciously read all caps as shouting, lots of full sentences punctuated by periods as overly serious, and ellipsis as an unfinished or perhaps even disapproving thought, depending on context. However, I have plenty of coworkers who do all these things with an opposite or orthogonal intentions, which requires quite a bit of effort on my part to not misinterpret. Ultimately, I just end up mapping the particular style of texting to the person do get a better idea of how they're feeling, but this takes time and isn't foolproof.

## The quality of English should be independent of the quality of the idea.
The other thing I've been thinking of, especially in light of the movie & book, is separating the quality of an idea from the delivery and language surrounding it. Speaking English well should be independent of idea quality, yet I find myself frequently mixing the two like the former predicts the later.

English is rarely even the right medium for most ideas (perhaps a picture?), so it's especially confusing. I don't think I'm alone with this unwarranted correlation, and I think the movie, "Don't Look Up" is an excellent example. It has a charismatic female president who is basically a thinly veiled Donald Trump. Her plans are often fatalistically simple, but her political machinations are as deft as ever, and is an excellent example of the importance of separating idea from message; the feel that permeates rallies is powerful, persuasive, and seductive, but the message ("Don't Look Up") is absurdly contrarian and short-sighted, only laid bare when the literal meteor flies above.

This idea also works in reverse -- people with strong accents or poor command of the English language have great ideas too. Plenty of smart people speak broken English, but it doesn't make their ideas, independent of the English, any worse. However, for most audiences, myself (sometimes!)included, it can tarnish the speaker and make it much more difficult to understand the true ingenuity or novelty of a solution. This is something I am working to make myself aware of and help others to understand.


## TL;DR
If nothing else, I think many native English speakers would do well to understand their audience a little better, especially when writing or talking to non-native speakers. I also think that the quality of English is independent of the quality of the idea inside, for better or worse.
