---
title: The Issue with Non-Issues
menu: "writing"
date: 2020-12-27
---

*This is an essay I wrote while procrastinating on writing a different, less enjoyable essay. It's silly and irreverent*

The issue with non-issues.

Politics has always been a nasty game. Since the beginnings of democracy, politicians have been hurling insults (among other, more substantial things) at each other, and that certainly hasn’t changed today. For as much ado is made about the growing disenchantment, political polarization and the lack of “polish” on both sides, the rise of technology has simply allowed voters to see the uglier sides of politics.

Turn on the TV today, and regardless of channel, the other side might as well be the devil incarnate. Irrespective of ideology, members of each party dutifully turn out to support their nominee, without any apparent thought to what their candidate says or does. It’s blind party allegiance, not predicated on issues but on red and blue and re-election cycles. 

It’s more or less always been this way -- the “corrupt” bargain of 1824 ended in Adams being elected over Jackson, despite Jackson technically winning a larger share of the electorate. In 1876, Hayes won over Tilden, by a single electoral vote, yet with Tilden winning nearly 4% more of the popular vote -- a total of nearly 250,000 voters. Lyndon B Johnson had a particular fascination with exposing himself, particularly to members of Congress, and Lincoln openly referred to Stevens during the debates as a bloodthirsty dog.

One of those men is considered a Founding Father, two are on US currency, and the fourth the father of a landmark Civil Rights bill.

The biggest difference is not in the candidates running, but rather what the American public sees -- no longer  incessantly primped and polished, the public instead gets a closer look at the candidates themselves. If Nixon was nervous during the first televised debate, imagine him in an age of Snapchat, Youtube and Facebook -- he would be a sweaty, stammering wreck. 

I’ll be able to vote this cycle, and despite what’s being said, this is shaping up to be one of the most interesting years yet, in spite of the negativity. A fairly weak third party candidate is polling at nearly 10%, a self-avowed socialist came unprecedentedly close to winning the Democratic primary, and most Americans still don’t know what the Electoral College is. It’s a year for the textbooks; I think it would be downright irresponsible not to participate.

I took a few Saturdays to actually go out and canvass during the primaries. I received everything from enthusiastic support to one person threatening to shoot me if I didn’t promptly leave, all within approximately one square mile. It’s an baffling amount of diversity, and an experience I certainly won’t forget. 

Even if the candidates historically haven’t changed much in character, the public’s access and ability to dissect their statements has never been greater. To say this election is fundamentally different misses both the richness of the game and it’s comparatively mild place in history.


Politics has always been dirty -- if it wasn’t, it wouldn’t be any fun. 
