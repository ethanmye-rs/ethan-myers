---
Title: code
date: 2020-12-27T21:52:29-07:00
draft: false
description: "The landing page for ethan.id's code page"
weight: 2
---

I enjoy writing code. I've collected some of my favorite code fragments and tricks, mostly in python, here. It also includes some of my home infrastructure and devices I use.
