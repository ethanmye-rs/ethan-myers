---
Title: Site Infrastructure
date: 2021-07-26T21:52:29-07:00
menu: "code"
bibFile: content/code/hardware.json
---

# Why?

A mixture of curiosity and frustration -- the negligent stepmother of invention. For whatever masochistic reason, I like playing with new software; setting up demos is a great way to understand more about systems I would otherwise not be exposed to. Apart from curiosity, I also find the alternatives quite frustrating. Most replacement services (Google Drive, Dropbox, etc) are engineered to turn non-paying customers into paying customers. They're a business, I get it! However, with a little bit of money upfront, you can forever ignore that tiny bit of friction inherent in free services and make life better.

I also strongly dislike the rent-seeking behavior that seems to be the predominate business model today and refuse to support companies that employ it.

# Hardware

## Server
A [Gen 10 HP Enterprise Microserver](https://support.hpe.com/hpesc/public/docDisplay?docLocale=en_US&docId=emr_na-a00028524en_us) forms the heart of the system. It's a wonderful little device -- small, quiet and energy efficient. It has space for 4 3.5" SATA drives, and if you hijack the disc reader SATA port, space for a 5th slim SSD. There's no iLo, and it's generally inferior to the Gen 10 Plus, but I got it for dirt cheap! The RAM (8GB stock) is limited, but this is easily extended.

## Drives
I'm using 4 HGST HUA723030ALA640 3TB I bought used for $30 a piece. They arrived with about 46k hours each. As of early 2021, they are at about 53k hours (`sudo smartctl --all /dev/sdx`) As enterprise drives, they're expected to run 24/7, and their [MTBF spec](https://documents.westerndigital.com/content/dam/doc-library/en_us/assets/public/western-digital/product/data-center-drives/ultrastar-sata-series/data-sheet-ultrastar-7k3000.pdf) is nearly 2M hours. Considering my usage is far less intensive than a datacenter, I expect to replace the drives for capacity reasons long before they fail.

# Software
I'm running Ubuntu 20.02 LTS. I also considered Debian, but Ubuntu was the first Linux distro I ever used and I think their documentation and support base is excellent. Plus, Ubuntu Server is basically Debian with all the packages I was going to install anyways.

## This site
This site is generated with Hugo, using a theme I designed. It tries to respect the system theme, and has no toggle for dark/light. I couldn't figure out how to do it simply without localStorage or a cookie, and I wasn't about to introduce that for something so small.

On a separate note, I dislike website that have huge margins, so my margins are generally 90+%. I paid for a 15.6" screen, might as well use it.

I've incorporated several Hugo shortcodes to allow for split columns (see [books](ethan.id/books/)), and Katex (only loading if needed) from an external CDN to render math.

I've also included schema to allow google to link subcategories, google "ethan myers id" to see what I'm talking about.

## ZFS
For storage, I'm using ZFS. This lets me run a pool with two mirrors, each with two drives in it. This provides some basic redundancy -- if any one drive fails, the other mirror has a copy of the data and I can simply replace the failed disk with a new drive. However, if two drives fail in separate pools, the data is lost.

![Screenschot of zpool status && zpool status][v2]

## Dynamic DNS
This site runs on a non-static residential IP, which means my IP changes from time to time. Because the domain name is mapped to an IP, something needs to periodically update my IP to make sure my domain name points to the right IP. My registrar has a dynamic DNS service with a endpoint you can POST updated values, which makes a periodic cron job or a smarter script possible.

# Future Plans
As part of that, I want to explore Hugo's image processing to super compress the existing image sizes to make the page smaller and smaller, along with asynchronous loading.


[v2]: /img/code/homehardware/zpool.PNG





