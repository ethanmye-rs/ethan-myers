---
title: Short Tricks in Python
date: 2020-12-27T21:52:29-07:00
menu: "code"
---

## Writing big numbers nicely

Python will interpret `100_000_000_000` as `100000000000`. I think the former is much easier to read.

	>>> 100_000_000_000 is 100000000000 & 100_000_000_000 == 100000000000
	True

This is quite similar to how in C++ you can use ``1`000`000`` to write large numbers

## Quickly commenting a block of code
Python has no block comments. Instead, use `CTRL` + `\` on a highlighted block of text to comment it out. Most editors seem to support this.

## Print convenience concatenation
Python will concatenate things for you. Sometimes very annoying, sometimes helpful:

	>>> print("Ethan " "Myers")
	Ethan Myers

## Easter Eggs
	>>> import antigravity
	>>> import this

## Walrus Operators
Turn a assign/return 2 liner into a one line walrus operator. This is new in python 3.8.
This code:

	>>> newshizzle = False
	>>> print(newshizzle)
is now 

	>>> print(newshizzle := True)