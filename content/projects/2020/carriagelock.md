---
Title: Designing a Carriage Lock
date: 2020-06-02T21:24:37-07:00
description: "Designing and machining a simple aluminum carriage lock for an 7x14 mini lathe"
menu: "projects"
draft: false
---

{{< youtube W3iqkKz2sO0 >}}
*Short demo of the clamping effectiveness*

A simple part, but the first real upgrade I've made for my lathe. The top is 30.1mm x 25.50mm wide, while the base is 30.1mm x 39.1mm wide. Total thickness is 12.75mm, and the shoulders are 8mm tall. The only critical dimension here is the top 25.50 measurement -- Ideally, it's tight enough to not wobble as it moves through the bed, but not so tight as to bind. I found my bed to very loosely (and coarsely!) machined. It's something I hope to clean up in the future, but it not critical.

![Finished][1]
*The finished part*

[1]: /img/2020/carriageclamp/finished.jpg



