---
title: Rogowski Coil
date: 2020-12-27T21:24:37-07:00
draft: false
menu: "projects"
---

![microscope view][1]
*0.12mm wire, cleanly wrapped*

## Summary

This project was a failure in execution, but a success is final product. In short, winding 3k perfect turns of 0.1mm wire is difficult, but doable (although not twice). Good ebay-fu reveals a [cheaper alternative](http://en.etcr.com.cn/product/info?id=768) with way less work.

I'm building a spot welder and want a way to reliable capture and quantify the pulse of current flowing into the weld. Unfortunately, the current is on the order of single kA, and estimations rapidly break down with wildly fluctuating voltage, temperature, and chemistry. Thus, I need to way to see actual peak current flowing into the weld so I can compute weld energy.

## Other Approaches

A shunt resistor is cheap, but intrudes on the test significantly and if not appropriately sized, may just melt or pop.
A current transformer seems like the easiest choice, but finding an appropriately sized transformer that won't run into saturation issues is difficult and expensive.
A Rogowski coil has no saturation issues, is flexible, and in theory, cheap. It does return the time derivative of the current, which requires integration.

## Rogowski Coil

[Wikipedia](https://en.wikipedia.org/wiki/Rogowski_coil) has an excellent summary on Rogowski coils, and I used the calculation there when designing my coil. Critically, the Rogowski coil is a torroid -- that is, it's a bunch of little loops around a big loop.





I used 36 awg (0.127mm diamter) wire and 1/2" (6.35mm) diameter clear, flexible PVC. I aimed for a 500mV output swing, and estimated  to be 500,000. I chose R to be 50.8mm, and a 315mm circumference. Plugging this all in, I got a 2000 turn estimate. The actual number really isn't critical, as all construction values bascially turn into a constant in front of the 





## Build

This thing was a PITA to make. Early attempts to wind by hand were slow, inconsistent, and generally just looked bad. Since the PVC tube is flexible, it needs to be supported by a mandrel. In early testing, I had no problem removing the pvc from the mandrel, but I failed to consider the appreciable compression from the windings. 

![early][3]
*An early attempt. Wavy, inconsistent, and generally ugly*

I eventually settled on using my lathe to wind the coils. I wanted to use the carriage feed of my lathe to wind the threads, but the pitch was slightly off and I ended up with overlapping threads every few cm. I opted to wind it by hand, and used a microscope to view the winding process up close. While this process is probably doable without it, it would be extremely difficult. Additionally, I printed a spool guide to hold the spool and guide it toward the mandrel. This made it easy for me to guide the wire to the PVC.

I used UV curing glue to seal the windings to the PVC. The UV glue bonded extremely well to PVC, which was surprising considering the low surface energy. It's a very handy glue to have.

![Setup][2]
*Setup showing microscope, lathe, and 3d printed spool guide*




## Commercial Alternative

![ectr][4]

This is a commercial Rogowski Probe I purchased for $25 on eBay from [ECTR](http://en.etcr.com.cn/product/info?id=768). While a used iFlex from Fluke can be had for a little more ($50 or so), I had no desire to wait for a good condition one to pop up.

Once I have the probe in hand, I'll update with some measurements.

## Resources

[This paper from TI](http://www.ti.com/lit/ug/tidubv4a/tidubv4a.pdf?ts=1591120230328)

[This paper from Keysight](https://community.keysight.com/community/keysight-blogs/oscilloscopes/blog/2017/11/29/what-is-a-rogowski-coil-current-probe)

[This poster from EMRP](http://projects.npl.co.uk/power_energy/docs/7%20Design%20and%20Calibration%20of%20Rogowski%20Coils.pdf)




[1]: /img/2020/rogowski/mic2.jpg
[2]: /img/2020/rogowski/setup1.jpg
[3]: /img/2020/rogowski/early.jpg
[4]: /img/2020/rogowski/ECTR.jpg