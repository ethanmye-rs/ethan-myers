---
title: "projects"
date: 2020-12-27
draft: false
description: "The landing page for ethan.id's project page ethan myers"
weight: 1
---

Working on projects is a labor of love. It better be, because they are, in absolute terms, horrendously unproductive and hideously expensive. 

However, this is all not a waste. I've been fortunate to learn so much in school, from excellent teachers who, for the most part, care. That being said, I've learned 80+% of what I know on my own. Whether is be math, PCB design, machining, programming, or cooking, I found a great common base provided by school (mostly math and English) and enough free time to put it to use. So many people malign school for useless content, but fail to see just how pervasive it is; communication is so much easier when everyone has a basic competency. 

Building a project forces me to understand every little bit of a design. It's easy to gloss over detail in a book; I frequently find myself believing with all my heart a concept, then being forced to prove it, and becoming convinced I've been tricked. Trick or not, there is a massive difference in understanding from those who do and those who see. I believe with all my heart that I do not understand until I do.  
